package com.dh.didemo.Controller;

import org.springframework.stereotype.Controller;

@Controller
public class MyController {
    public String hello() {
        System.out.println("Hello Spring");
        return "Hello Spring";
    }
}
