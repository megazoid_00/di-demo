package com.dh.didemo;

import com.dh.didemo.Controller.ConstructorBasedController;
import com.dh.didemo.Controller.GetterBasedController;
import com.dh.didemo.Controller.MyController;
import com.dh.didemo.Controller.PropertyBasedController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource("classpath:beanconfig.xml")
public class DiDemoApplication {

    public static void main(String[] args) {
        //SpringApplication.run(DiDemoApplication.class, args);
        ApplicationContext context = SpringApplication.run(DiDemoApplication.class, args);
        MyController controller = (MyController) context.getBean("myController");
        controller.hello();

        System.out.println(context.getBean(PropertyBasedController.class).sayHello());
        System.out.println(context.getBean(GetterBasedController.class).sayHello());
        System.out.println(context.getBean(ConstructorBasedController.class).sayHello());
        System.out.println(context.getBean(Forecast.class).weather());
        System.out.println(context.getBean(FakeDataSource.class).getUser());
        System.out.println(context.getBean(FakeDataSource.class).getPassword());
        System.out.println(context.getBean(FakeJMS.class).getUser());
    }
}
